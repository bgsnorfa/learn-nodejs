class User {
  _id;
  _firstName;
  _lastName;
  _email;
  _password;

  constructor(email, password) {
    this._email = email;
    this._password = password;
  }

  login(email, password) {
    if (this._email === email && this._password === password) {
      return true;
    } else {
      return false;
    }
  }

  setFirstName(firstName) {
    this._firstName = firstName;
  }

  setLastName(lastName) {
    this._lastName = lastName;
  }

  getFirstName() {
    return this._firstName;
  }

  getLastName() {
    return this._lastName;
  }

  setId(id) {
    this._id = id;
  }

  getId() {
    return this._id;
  }
}

module.exports = User;