class UserList {
  _lists = [];
  _lastId = 0;

  constructor() {}

  addUser(User) {
    this._updateLastId();
    User.setId(this._getLastId());
    this._lists.push(User);
  }

  getUsers() {
    return this._lists;
  }

  _updateLastId() {
    this._lastId = this._lastId + 1;
  }

  _getLastId() {
    return this._lastId;
  }
}

module.exports = UserList;