const User = require("./User/User");
let UserList = require("./User/UserList");

let userLists = new UserList();

for (let i = 1; i <= 5; i++) {
  let user = new User(`test${i}@mail.com`, "123456");
  user.setFirstName(`User ${i}`);
  user.setLastName("Test");
  userLists.addUser(user);
}

console.table(userLists.getUsers());